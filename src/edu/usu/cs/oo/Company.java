package edu.usu.cs.oo;

public class Company {

	private String name;
	private Location location;
	
	public Company(String inName, Location inLocation)
	{
		name = inName;
		location = inLocation;
	}
	
	public Company()
	{
		name = "Unknown";
		location = new Location();
	
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	
	public String toString()
	{
		return "Company Name: " + name + ", Location: " + location.toString();
	}
	
	/*
	 * Create the constructor, getters, setters, and anything else
	 * that is necessary to make Company work.
	 * 
	 * Note: This includes the toString() method.
	 */
}

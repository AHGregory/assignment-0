package edu.usu.cs.oo;

public class Location {

	private String streetAddress;
	private String areaCode;
	private String city;
	private String state;
	
	
	public Location(String address, String zipCode, String cityOrTown, String whatState)
	{
		streetAddress = address;
		areaCode = zipCode;
		city = cityOrTown;
		state = whatState;
	}
	
	public Location()
	{
		streetAddress = "unknown";
		areaCode = "unknown";
		city = "unknown";
		state = "unknown";
	}
	
	
	public String toString()
	{
		return "Address: " + streetAddress + ", City: " + city +", State: " + state + ", Zip Code: " + areaCode;
	}

	/*
	 * Create a constructor here that allows you to initialize a Location
	 * with streetAddress, areaCode, city, and state.
	 */
	
	
	/*
	 * Create getters and setters here for retrieving the private member variables here. 
	 */
	
	
	
	public String getStreetAddress() {
		return streetAddress;
	}
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}
	public String getAreaCode() {
		return areaCode;
	}
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
}

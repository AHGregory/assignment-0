package edu.usu.cs.oo;

public class Driver {
	
	public static void main(String[] args)
	{
		Student student = new Student("Chris Thatcher", "AOneMillion", new Job("Dentist", 100000, new Company()));
		
		/*
		 * Instantiate an instance of Student that includes your own personal information
		 * Feel free to make up information if you would like.
		 */
		Student myself = new Student("Adam Gregory", "A00925032", new Job("Professional Croquet Player", 99999, new Company("Geo Motor Company", new Location("1234 MotorOil Rd", "55555", "Detroit", "MI"))));
		
		System.out.println(student);
		System.out.println(myself);
		
		/*
		 * Print out the student information. 
		 */
	}

}